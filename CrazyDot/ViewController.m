//
//  ViewController.m
//  CrazyDot
//
//  Created by dor on 16.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import "ViewController.h"
#import "Circle.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <pop/POP.h>

@interface ViewController ()

@property BOOL getBackAnimEnded;

@property (strong, nonatomic) Circle *dot;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _dot = [ [ Circle alloc ] initWithFrame:CGRectMake(window_center_x-25, window_center_y-25, 50, 50) ];
    
    [ self.view addSubview:_dot ];
    
    UIPanGestureRecognizer *panGRec = [ [ UIPanGestureRecognizer alloc ] initWithTarget:self action:@selector(PanHandler:) ];
    
    [ _dot addGestureRecognizer:panGRec ];
    
    _getBackAnimEnded = true;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)PanHandler:(UIPanGestureRecognizer *)recognizer {
    CGPoint location = [ recognizer velocityInView:self.view ];
    
    POPSpringAnimation *animationScaleUp = [ POPSpringAnimation animation ];
    POPSpringAnimation *animationScaleDown = [ POPSpringAnimation animation ];
    POPSpringAnimation *animationAlphaUp = [ POPSpringAnimation animation ];
    POPSpringAnimation *animationAlphaDown = [ POPSpringAnimation animation ];
    POPDecayAnimation *animationMove = [ POPDecayAnimation animation ];
    
    animationScaleUp.property = [ POPAnimatableProperty propertyWithName:kPOPViewScaleXY ];
    animationScaleUp.toValue = [ NSValue valueWithCGSize:CGSizeMake(2, 2) ];
    animationScaleUp.removedOnCompletion = YES;
    animationScaleUp.springSpeed = 15.;
    animationScaleUp.springBounciness = 20;

    
    animationScaleDown.property = [ POPAnimatableProperty propertyWithName:kPOPViewScaleXY ];
    animationScaleDown.toValue =  [ NSValue valueWithCGSize:CGSizeMake(1, 1) ];;
    animationScaleDown.fromValue = [ NSValue valueWithCGSize:CGSizeMake(2, 2) ];
    animationScaleDown.springSpeed = 15.;
    animationScaleDown.springBounciness = 20.;
    
    animationAlphaUp.property = [ POPAnimatableProperty propertyWithName:kPOPViewAlpha ];
    animationAlphaUp.toValue = @(1);
    
    animationAlphaDown.property = [ POPAnimatableProperty propertyWithName:kPOPViewAlpha ];
    animationAlphaDown.toValue = @(0.5);
    
    animationMove.property = [ POPAnimatableProperty propertyWithName:kPOPViewCenter ];
    animationMove.velocity = [ NSValue valueWithCGRect:CGRectMake(location.x, location.y, 50, 50) ];
    
    switch ( recognizer.state ) {
        case UIGestureRecognizerStateBegan:
            [ _dot pop_addAnimation:animationScaleUp forKey:@"ScaleDotUp" ];
            [ _dot pop_addAnimation:animationAlphaDown forKey:@"DotAlphaDown" ];
            break;
        
        case UIGestureRecognizerStateEnded:
            [ _dot pop_removeAllAnimations ];
            [ _dot pop_addAnimation:animationScaleDown forKey:@"ScaleDotDown" ];
            [ _dot pop_addAnimation:animationAlphaUp forKey:@"DotAlphaUp" ];
            break;
    }
   
    [ _dot pop_addAnimation:animationMove forKey:@"MoveItUp" ];
    
    
    [[RACObserve(_dot, center) deliverOnMainThread] subscribeNext:^(NSValue *centerValue) {
        if(!CGRectIntersectsRect(_dot.frame, self.view.frame)) {
            if ( _getBackAnimEnded ) {
            
                POPSpringAnimation *animationGetBack = [ POPSpringAnimation animationWithPropertyNamed:kPOPViewCenter ];
            
                animationGetBack.toValue = [ NSValue valueWithCGPoint:self.view.center ];
                animationGetBack.springBounciness = 20.;
                animationGetBack.springSpeed = 15.;
                
                animationGetBack.completionBlock = ^void(POPAnimation *animation, BOOL completed) {
                    _getBackAnimEnded = true;
                };
            
                [ _dot pop_removeAnimationForKey:@"MoveItUp" ];
                [ _dot pop_addAnimation:animationGetBack forKey:@"GetDotBack" ];
            
                _getBackAnimEnded = false;
            }
        }
    }];
    
    
    }

@end
