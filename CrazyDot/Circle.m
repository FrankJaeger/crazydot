//
//  Circle.m
//  CrazyDot
//
//  Created by dor on 16.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import "Circle.h"

@implementation Circle

- (id)initWithFrame:(CGRect)frame {
    self = [ super initWithFrame:frame ];
    
    self.layer.cornerRadius = frame.size.width/2;
    self.backgroundColor = [UIColor colorWithRed:0.39 green:0.58 blue:0.97 alpha:1.0];
    
    return self;
}

@end
