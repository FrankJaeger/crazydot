//
//  Circle.h
//  CrazyDot
//
//  Created by dor on 16.04.2015.
//  Copyright (c) 2015 dor. All rights reserved.
//

#import <UIKit/UIKit.h>

#define window_center_x [ UIScreen mainScreen ].bounds.size.width/2
#define window_center_y [ UIScreen mainScreen ].bounds.size.height/2

@interface Circle : UIView

@end
